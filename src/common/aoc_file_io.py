"""Functions  to handle common File IO tasks for AoC Challenges."""


def read_file_as_str(file_path) -> str:
    """
    Open a file as read only and return as a string.

    Will strip whitespace from either end of the string.
    """
    if file_path.is_file():
        with open(file_path, "r", encoding="utf-8") as input_file:
            input_str = input_file.read().strip()
    else:
        raise FileNotFoundError("The file path does not exist:", file_path)
    return input_str


def str_to_list_of_str(input_str, split_char) -> list[str]:
    """Return a list of strings, new string each time split_char is seen."""
    return list(input_str.split(split_char))


def list_of_str_to_list_of_int(list_of_strings) -> list[int]:
    """
    Accept a list of strings, return a list of ints.

    map() executes a specified function for each item in an iterable.
    """
    return list(map(int, list_of_strings))


def read_file_as_list_of_str(file_path, delimiter="\n") -> list[str]:
    """
    Return file contents as a list of strings split at delimiter.

    Strips any whitespace from either end of the string.
    Converts to a list of strings, new string each time delimiter is seen.
    """
    return str_to_list_of_str(read_file_as_str(file_path), delimiter)


def read_file_as_list_of_int(file_path, delimiter="\n") -> list[int]:
    """
    Return file contents as a list of integers split at delimiter.

    Strips any whitespace from either end of the string.
    Converts to a list of strings, new string each time delimiter is seen.
    Converts the list of strings to a list of ints, and returns this.
    """
    puzzle_input = str_to_list_of_str(read_file_as_str(file_path), delimiter)
    return list_of_str_to_list_of_int(puzzle_input)
