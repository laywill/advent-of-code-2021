import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_07 import day07  # noqa: E402


def test_part1():
    # Define the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_07/day07_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, ",")
    result = day07.part1(input_test)
    expected = 37
    assert result == expected


def test_part2():
    # Define the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_07/day07_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, ",")
    result = day07.part2(input_test)
    expected = 168
    assert result == expected
