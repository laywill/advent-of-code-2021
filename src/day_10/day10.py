#!/usr/bin/env python3.9
"""Solution to Day 10 of AoC 2021."""

import os
import sys
from collections import deque
from pathlib import Path
from statistics import median

# import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def part1(incorect_closing_ch_list: list) -> int:
    """Return solution to Part 1."""
    points_dict = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137,
    }

    part1_answer = 0
    for char in incorect_closing_ch_list:
        part1_answer += points_dict[char]
    print("Day 10 - Part 1:", part1_answer)

    return part1_answer


def part2(valid_lines_list: list) -> int:
    """Return solution to Part 2 given list of valid but incomplete lines."""
    points_dict = {
        "(": 1,
        "[": 2,
        "{": 3,
        "<": 4,
    }

    results = []
    for line in valid_lines_list:
        line.reverse()
        line_result = 0
        for char in line:
            line_result = (line_result * 5) + points_dict[char]
        results.append(line_result)

    part2_answer = int(median(results))
    print("Day 10 - Part 2:", part2_answer)

    return part2_answer


def main(input_data):
    """
    Return solutions to Day10 part 1 and part 2.

    Accepts a list of strings as input, one string for each line in puzzle input.
    """
    incorect_closing_ch = []
    valid_lines = []
    for _, line in enumerate(input_data):
        # deque provides O(1) time complexity for append() and pop() compared to
        # list  provides O(n) time complexity.
        line_ch = deque()
        something_incorrect = False
        for _, char in enumerate(line):
            if char in ["[", "(", "{", "<"]:
                line_ch.append(char)
            elif char == "]":
                if line_ch.pop() != "[":
                    incorect_closing_ch.append(char)
                    something_incorrect = True
            elif char == ")":
                if line_ch.pop() != "(":
                    incorect_closing_ch.append(char)
                    something_incorrect = True
            elif char == "}":
                if line_ch.pop() != "{":
                    incorect_closing_ch.append(char)
                    something_incorrect = True
            elif char == ">":
                if line_ch.pop() != "<":
                    incorect_closing_ch.append(char)
                    something_incorrect = True

        if something_incorrect is False:
            valid_lines.append(list(line_ch))

    part1_result = part1(incorect_closing_ch)
    part2_result = part2(valid_lines)

    return part1_result, part2_result


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_10/day10_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_10/day10_real.txt"
    # input_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)
    input_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_DATA)

    # main(input_test)
    main(input_real)
