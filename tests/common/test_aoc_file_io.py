import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402


def test_read_file_as_str():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_01/day01_test.txt"

    result = aoc_file_io.read_file_as_str(INPUT_PATH_TEST)
    expected = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263"

    assert result == expected


def test_str_to_list_of_str():
    test_string = "This is a test string"
    delimiter = " "

    result = aoc_file_io.str_to_list_of_str(test_string, delimiter)
    expected = ["This", "is", "a", "test", "string"]

    assert result == expected


def test_list_of_str_to_list_of_int():
    test_input = ["1", "2", "3", "4", "5"]

    result = aoc_file_io.list_of_str_to_list_of_int(test_input)
    expected = [1, 2, 3, 4, 5]

    assert result == expected


def test_read_file_as_list_of_str():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_01/day01_test.txt"

    result = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST, "\n")
    expected = [
        "199",
        "200",
        "208",
        "210",
        "200",
        "207",
        "240",
        "269",
        "260",
        "263",
    ]

    assert result == expected


def test_read_file_as_list_of_int():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_01/day01_test.txt"

    result = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, "\n")
    expected = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]

    assert result == expected
