#!/usr/bin/env python3.9
"""Solution to Day 3 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def most_common(lst: list) -> str:
    """Return whether there were more ones or zeros in a list."""
    # Works for Part1, but produces an off-by-one error in Part 2
    # return max(set(lst), key=lst.count)
    ones = lst.count("1")
    zeros = lst.count("0")
    return "1" if ones >= zeros else "0"


def least_common(lst: list) -> str:
    """Return whether there were fewer ones or zeros in a list."""
    # Works for Part1, but produces an off-by-one error in Part 2
    # return min(set(lst), key=lst.count)
    ones = lst.count("1")
    zeros = lst.count("0")
    return "0" if zeros <= ones else "1"


def part1(binary_numbers: [str]) -> int:
    """Return Power Consumption (product of gamma_rate and epsilon_rate)."""
    gamma_rate = []
    epsilon_rate = []

    # Filter the lists to only include those with the most / least common elements
    for binary_idx, _ in enumerate(binary_numbers[0]):
        element_list = []
        for element_idx, _ in enumerate(binary_numbers):
            element_list.append(binary_numbers[element_idx][binary_idx])

        gamma_rate.append(most_common(element_list))
        epsilon_rate.append(least_common(element_list))

    # Convert binary to integer
    gamma_rate_int = int(''.join(gamma_rate), 2)
    epsilon_rate_int = int(''.join(epsilon_rate), 2)

    # Return the solution to part 1: the power consumption
    return gamma_rate_int * epsilon_rate_int


def apply_filter(binary_list: list[str], index: int, value: int) -> list[str]:
    """Return list of binary strings, filter on if a digit is at index."""
    filtered = list(
        filter(lambda element: element[index] == value, binary_list)
    )
    return filtered


def filter_to_get_rating(binary_list: list[str], most_or_least_common) -> str:
    """Take a list, filter it from left to right by most or least common."""
    # Working from leftmost bit to rightmost bit in each binary value...
    for binary_idx, _ in enumerate(binary_list[0]):
        # Create a list of the bits we want to analyse
        element_list = []
        for element_idx, _ in enumerate(binary_list):
            element_list.append(binary_list[element_idx][binary_idx])

        # Call the function passed as an argument, passing the newly created list
        # to work out if we will need to filter on "1" or "0"
        filter_by = most_or_least_common(element_list)

        # Now apply the filter to the list.
        binary_list = apply_filter(binary_list, binary_idx, filter_by)

        # We are done when we only have one value left in the list.
        if len(binary_list) == 1:
            break
    return binary_list[0]


# def oxygen_str(binary_numbers: [str]) -> str:
def oxygen_str(binary_numbers_list: list[str]) -> str:
    """Return oxygen_generator_rating as a binary string."""
    return filter_to_get_rating(binary_numbers_list, most_common)


def co2_str(binary_numbers_list: list[str]) -> str:
    """Return co2_scrubber_rating as a binary string."""
    return filter_to_get_rating(binary_numbers_list, least_common)


def part2(binary_numbers: list[str]) -> int:
    """Return life support rating (product of oxygen_generator_rating and co2_scrubber_rating)."""
    # Convert binary to integer
    oxygen_generator_rating_int = int(oxygen_str(binary_numbers), 2)
    co2_scrubber_rating_int = int(co2_str(binary_numbers), 2)

    # Return the solution to part two: the life support rating
    return oxygen_generator_rating_int * co2_scrubber_rating_int


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    INPUT_PATH_DATA = Path.cwd() / "./data/day_03/day03_real.txt"
    input_real = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_DATA)

    print("Day 03 - Part 1:", part1(input_real))
    print()
    print("Day 03 - Part 2:", part2(input_real))
