#!/usr/bin/env python3.9
"""Solution to Day 2 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def read_file_as_list(file_path) -> list[tuple[str, int]]:
    """
    Return file contents as a list of commands.

    Strips any whitespace from either end of the string.
    Converts to a list of strings, new string each time delimiter is seen.
    Converts the list of strings to a list of ints, and returns this.
    """
    # Read file as string and strip whitespace
    ip_str = aoc_file_io.read_file_as_str(file_path)

    # Convert string to list seperated by delimiter
    list_of_strings = list(ip_str.split("\n"))

    # Split each element in the list into a string and an int
    list_of_commands = []
    for element in list_of_strings:
        element_split = list(element.split(" "))
        element_split[1] = int(element_split[1])
        list_of_commands.append(tuple(element_split))

    # Pass back list of lists, each containing str and int
    return list_of_commands


def part1(command_list: list) -> int:
    """Return product of final depth and horizontal position."""
    pos_h = 0
    depth = 0

    for command in command_list:
        if command[0] == "forward":
            pos_h += command[1]
        elif command[0] == "up":
            depth -= command[1]
        elif command[0] == "down":
            depth += command[1]
        else:
            raise ValueError("Unknown command supplied")

    product = pos_h * depth
    return product


def part2(command_list: list) -> int:
    """Return product of final depth and horizontal position."""
    pos_h = 0
    depth = 0
    aim = 0

    for command in command_list:
        if command[0] == "forward":
            pos_h += command[1]
            depth += aim * command[1]
        elif command[0] == "up":
            aim -= command[1]
        elif command[0] == "down":
            aim += command[1]
        else:
            raise ValueError("Unknown command supplied")

    product = pos_h * depth
    return product


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_02/day02_real.txt"

    # Read the files and convert to lists of ints
    commands_real = read_file_as_list(INPUT_PATH_DATA)

    print("Day 02 - Part 1:", part1(commands_real))
    print("Day 02 - Part 2:", part2(commands_real))
