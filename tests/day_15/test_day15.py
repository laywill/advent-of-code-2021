import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_15 import day15  # noqa: E402


def test_import_data():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    expected = np.array(
        [
            [1, 1, 6, 3, 7, 5, 1, 7, 4, 2],
            [1, 3, 8, 1, 3, 7, 3, 6, 7, 2],
            [2, 1, 3, 6, 5, 1, 1, 3, 2, 8],
            [3, 6, 9, 4, 9, 3, 1, 5, 6, 9],
            [7, 4, 6, 3, 4, 1, 7, 1, 1, 1],
            [1, 3, 1, 9, 1, 2, 8, 1, 3, 7],
            [1, 3, 5, 9, 9, 1, 2, 4, 2, 1],
            [3, 1, 2, 5, 4, 2, 1, 6, 3, 9],
            [1, 2, 9, 3, 1, 3, 8, 5, 2, 1],
            [2, 3, 1, 1, 9, 4, 4, 5, 8, 1],
        ]
    )
    assert (data == expected).all()


def test_expand_board():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    data_expanded = day15.expand_board(data, 5, 5)

    INPUT_PATH_EXPECTED = Path.cwd() / "./tests/day_15/day15_test_expanded.txt"
    expected = day15.import_data(INPUT_PATH_EXPECTED)

    assert (data_expanded == expected).all()


def test_Graph():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    g = day15.Graph(data)

    assert (g.edges == data).all()
    assert len(g.visited) == 0
    assert len(g.cost_dict) == 0


def test_graph_djikstra():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    g = day15.Graph(data)
    g.dijkstra((0, 0))

    assert (g.edges == data).all()
    assert len(g.visited) == 100
    assert len(g.cost_dict) == 100


def test_find_total_cost():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    result = day15.find_total_cost(data)
    expected = 40
    assert result == expected


def test_find_total_cost_expanded():
    INPUT_PATH = Path.cwd() / "./data/day_15/day15_test.txt"
    data = day15.import_data(INPUT_PATH)
    data_expanded = day15.expand_board(data, 5, 5)
    result = day15.find_total_cost(data_expanded)
    expected = 315
    assert result == expected
