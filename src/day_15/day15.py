#!/usr/bin/env python3.9
"""Solution to Day 15 of AoC 2021."""

import os
import sys
from pathlib import Path
from queue import PriorityQueue

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_data(path_to_file) -> np.ndarray:
    """Load the input file and return a numpy array of ints."""
    input_as_list_of_str = aoc_file_io.read_file_as_list_of_str(path_to_file)
    board = np.array(
        [
            [int(num) for num in list(line.strip())]
            for line in input_as_list_of_str
        ]
    )
    return board


class Graph:  # pylint: disable=too-few-public-methods
    """Object to hold graph of vertices and edges to traverse."""

    def __init__(self, board: np.ndarray):
        """
        Initialise the object with variables that will be used later.

        edges:     Matrix of cost of entering node (u,v) = self.edges[u,v] = weight of the edge.
        visited:   A set which contains the visited vertices.
        cost_dict: A dictionary, with vertices as keys, the cost to traverse there as values.
        """
        self.edges = np.array(board)
        self.visited = set()
        self.cost_dict = {}

    def dijkstra(self, start_vertex: tuple):
        """
        Return dictionary of coords : cost pairs to traverse to a given location.

        Use Djikstra's algorithm to cost to get from the starting vertex to
        any other vertex in the graph.
        Here, verticies are given as XY coordinates in a matrix.

        Start vertex specifies the starting point to use as a coordinate (0,0)
        Representing (row, column)
        """
        # Shape = (rows, cols)
        g_shape = list(self.edges.shape)
        for row in range(g_shape[0]):
            for col in range(g_shape[1]):
                self.cost_dict[(row, col)] = float('inf')
        self.cost_dict[start_vertex] = 0

        index_to_check_queue = PriorityQueue()
        index_to_check_queue.put((0, start_vertex))

        while not index_to_check_queue.empty():
            (_, current_vertex) = index_to_check_queue.get()
            self.visited.add(current_vertex)

            # We only compare up, down, left and right from current_vertex
            # Express as 4 coordinate pairs to use as modifiers
            adjacent = [(-1, 0), (1, 0), (0, -1), (0, 1)]
            for _, modifier in enumerate(adjacent):
                row = current_vertex[0] + modifier[0]
                col = current_vertex[1] + modifier[1]
                if row < 0 or row > g_shape[0] - 1:
                    # Row out of bounds, skip
                    continue
                if col < 0 or col > g_shape[1] - 1:
                    # Col out of bounds, skip
                    continue

                neighbor = (row, col)
                if neighbor == current_vertex:
                    # Somehow we have ended up trying to compare with outselves
                    # Raise an error
                    raise ValueError("neighbor == current_vertex")

                distance = self.edges[neighbor]

                if neighbor not in self.visited:
                    old_cost = self.cost_dict[neighbor]
                    new_cost = self.cost_dict[current_vertex] + distance
                    if new_cost < old_cost:
                        index_to_check_queue.put((new_cost, neighbor))
                        self.cost_dict[neighbor] = new_cost

        return self.cost_dict


def find_total_cost(data: np.ndarray) -> int:
    """
    Return int, total cost of traversing the lowest cost route.

    Route is from top left (0,0) to the bottom right (-1, -1) coordinate.
    Cost is the sum total of each square entered!
    The cost of the starting square does not add to the cost.
    """
    # Generate a Graph of verticies and edges
    cave_graph = Graph(data)

    # Run Djikstra's algorithm to find routes to each vertex from given start
    cave_graph.dijkstra((0, 0))

    # Identify the end vertex to aim for
    end_coord = list(data.shape)
    end_coord[0] = end_coord[0] - 1
    end_coord[1] = end_coord[1] - 1
    end_coord = tuple(end_coord)

    # Return the cost of traversing to the desired destination
    return cave_graph.cost_dict[end_coord]


def expand_board(
    board: np.ndarray, copy_right: int, copy_down: int
) -> np.ndarray:
    """
    Return a numpy array that is a tiled version of original.

    Your original map tile repeats to the right and downward;
    each time the tile repeats to the right or downward,
    all of its risk levels are 1 higher than the tile immediately up or left of it.

    However, risk levels above 9 wrap back around to 1.
    o, if your original map had only 1 position with a risk level of 8,
    then that same position on each of the 25 total tiles would be as follows:

    8 9 1 2 3
    9 1 2 3 4
    1 2 3 4 5
    2 3 4 5 6
    3 4 5 6 7
    """
    new_board = None
    for row in range(copy_down):
        temp_row = None
        for col in range(copy_right):
            temp_board = board + (row + col)
            temp_board = np.where(temp_board > 9, temp_board - 9, temp_board)
            if type(temp_row).__module__ != np.__name__:
                temp_row = temp_board
            else:
                temp_row = np.hstack((temp_row, temp_board))
        if type(new_board).__module__ != np.__name__:
            new_board = temp_row
        else:
            new_board = np.vstack((new_board, temp_row))
    return new_board


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_15/day15_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_15/day15_real.txt"

    input_real = import_data(INPUT_PATH_DATA)
    print(
        "Day 15 - Real - Part 1: Distance from top left to bottom right is",
        find_total_cost(input_real),
    )

    input_real_expanded = expand_board(input_real, 5, 5)
    print(
        "Day 15 - Real - Part 1: Distance from top left to bottom right is",
        find_total_cost(input_real_expanded),
    )
