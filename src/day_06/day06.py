#!/usr/bin/env python3.9
"""Solution to Day 6 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def create_dict(initial_fish_list: list) -> dict:
    """Return a dict summarising the number of fish at each count value."""
    new_dict = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0}
    for i in range(8):
        new_dict[i] = initial_fish_list.count(i)
    return new_dict


def run_one_day(fish_dict: dict) -> dict:
    """
    Update the dict summarising the number of fish at each count value.

    Each day fish get one day closer to producing offspring.
    A fish that reproduces resets its timer to 6
    New offspring have their timer set to 8.
    """
    # Deep copy the input
    out_dict = dict(fish_dict)
    for i in range(8):
        out_dict[i] = out_dict[i + 1]
    out_dict[8] = int(fish_dict[0])
    out_dict[6] += int(fish_dict[0])
    return out_dict


def breed_fish(list_of_fish: list, num_days_to_iterate: int) -> int:
    """Return the number of fish you have after N days."""
    fish_count_dict = create_dict(list_of_fish)
    for _ in range(num_days_to_iterate):
        fish_count_dict = run_one_day(fish_count_dict)
    return sum(fish_count_dict.values())


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_06/day06_real.txt"
    input_real = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_DATA, ",")

    print("Day 06 - Part 1:", breed_fish(input_real, 80))
    print()
    print("Day 06 - Part 2:", breed_fish(input_real, 256))
