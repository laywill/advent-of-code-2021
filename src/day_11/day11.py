#!/usr/bin/env python3.9
"""Solution to Day 11 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_data(path_to_file) -> np.ndarray:
    """Load the input file and return a numpy array of ints."""
    input_as_list_of_str = aoc_file_io.read_file_as_list_of_str(path_to_file)
    board = np.array(
        [
            [int(num) for num in list(line.strip())]
            for line in input_as_list_of_str
        ]
    )
    return board


def step(flash_array: np.ndarray):
    """Return the updated array and number of flashes after processing one step."""
    # Initialise flash counter
    flash_count = 0

    # First, the energy level of each octopus increases by 1.
    flash_array = flash_array + 1

    indicies = np.asarray(np.where(flash_array == 10)).T
    indicies = set(tuple(v) for v in indicies)

    # Nearby cells
    nearby = {
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    }

    # Get the shape of the array to check boundary values
    a_shape = flash_array.shape

    while indicies:
        y, x = indicies.pop()  # pylint: disable=invalid-name
        flash_count += 1

        for d_y, d_x in nearby:
            y_dy = y + d_y
            x_dx = x + d_x

            # Filter out anything with a negative index
            if (
                x_dx < 0
                or y_dy < 0
                or x_dx > a_shape[1] - 1
                or y_dy > a_shape[0] - 1
            ):
                continue

            flash_array[y_dy, x_dx] += 1
            if flash_array[y_dy, x_dx] == 10:
                indicies.add((y_dy, x_dx))

    # Flash Octopuses
    indicies = np.asarray(np.where(flash_array > 9)).T
    # indicies = set(indicies.flatten())
    indicies = set(tuple(v) for v in indicies)
    while indicies:
        y, x = indicies.pop()  # pylint: disable=invalid-name

        flash_array[y, x] = 0

    # print(flash_array)

    return flash_array, flash_count


def steps(input_array: np.ndarray, steps_to_iterate: int) -> int:
    """Return the number of flashes that occur in a number of steps."""
    # Deep Copy
    input_array_copy = np.array(input_array)

    flashes = 0

    # print(input_array_copy)

    for _ in range(steps_to_iterate):
        # print("Step", step_num + 1)
        input_array_copy, new_flashes = step(input_array_copy)
        flashes += new_flashes
    return flashes


def step_till_sync(input_array: np.ndarray) -> int:
    """Continue stepping until all octopusses flash together."""
    # Deep Copy
    input_array_copy = np.array(input_array)

    flashes = 0

    # print(input_array_copy)
    step_num = 0
    # for step_num in range(steps):
    while True:
        step_num += 1
        # print("Step", step_num)
        input_array_copy, new_flashes = step(input_array_copy)
        flashes += new_flashes
        if not np.any(input_array_copy):
            # All zeros
            break
    return step_num


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_11/day11_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_11/day11_real.txt"

    # input_test = import_data(INPUT_PATH_TEST)
    input_real = import_data(INPUT_PATH_DATA)

    # print("Day 08 - Test - Part 1:", steps(input_test, 100))
    print("Day 08 - Real - Part 1:", steps(input_real, 100))
    print()
    # print("Day 08 - Test - Part 2:", step_till_sync(input_test))
    print("Day 08 - Real - Part 2:", step_till_sync(input_real))
