import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_11 import day11  # noqa: E402


def test_import_data():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_11/day11_test.txt"
    result = day11.import_data(INPUT_PATH_TEST)
    expected = np.array(
        [
            [5, 4, 8, 3, 1, 4, 3, 2, 2, 3],
            [2, 7, 4, 5, 8, 5, 4, 7, 1, 1],
            [5, 2, 6, 4, 5, 5, 6, 1, 7, 3],
            [6, 1, 4, 1, 3, 3, 6, 1, 4, 6],
            [6, 3, 5, 7, 3, 8, 5, 4, 7, 8],
            [4, 1, 6, 7, 5, 2, 4, 6, 4, 5],
            [2, 1, 7, 6, 8, 4, 1, 7, 2, 1],
            [6, 8, 8, 2, 8, 8, 1, 1, 3, 4],
            [4, 8, 4, 6, 8, 4, 8, 5, 5, 4],
            [5, 2, 8, 3, 7, 5, 1, 5, 2, 6],
        ]
    )
    assert (result == expected).all()


def test_step():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_11/day11_test.txt"
    input_test = day11.import_data(INPUT_PATH_TEST)

    # Take one step
    result_array, result_flashes = day11.step(input_test)
    expected_array = np.array(
        [
            [6, 5, 9, 4, 2, 5, 4, 3, 3, 4],
            [3, 8, 5, 6, 9, 6, 5, 8, 2, 2],
            [6, 3, 7, 5, 6, 6, 7, 2, 8, 4],
            [7, 2, 5, 2, 4, 4, 7, 2, 5, 7],
            [7, 4, 6, 8, 4, 9, 6, 5, 8, 9],
            [5, 2, 7, 8, 6, 3, 5, 7, 5, 6],
            [3, 2, 8, 7, 9, 5, 2, 8, 3, 2],
            [7, 9, 9, 3, 9, 9, 2, 2, 4, 5],
            [5, 9, 5, 7, 9, 5, 9, 6, 6, 5],
            [6, 3, 9, 4, 8, 6, 2, 6, 3, 7],
        ]
    )
    expected_flashes = 0
    assert (result_array == expected_array).all()
    assert result_flashes == expected_flashes

    # Take a second to check flash logic
    result_array, result_flashes = day11.step(result_array)
    expected_array = np.array(
        [
            [8, 8, 0, 7, 4, 7, 6, 5, 5, 5],
            [5, 0, 8, 9, 0, 8, 7, 0, 5, 4],
            [8, 5, 9, 7, 8, 8, 9, 6, 0, 8],
            [8, 4, 8, 5, 7, 6, 9, 6, 0, 0],
            [8, 7, 0, 0, 9, 0, 8, 8, 0, 0],
            [6, 6, 0, 0, 0, 8, 8, 9, 8, 9],
            [6, 8, 0, 0, 0, 0, 5, 9, 4, 3],
            [0, 0, 0, 0, 0, 0, 7, 4, 5, 6],
            [9, 0, 0, 0, 0, 0, 0, 8, 7, 6],
            [8, 7, 0, 0, 0, 0, 6, 8, 4, 8],
        ]
    )
    expected_flashes = 35
    assert (result_array == expected_array).all()
    assert result_flashes == expected_flashes


def test_steps():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_11/day11_test.txt"
    input_test = day11.import_data(INPUT_PATH_TEST)

    # Take 100 steps
    result = day11.steps(input_test, 100)
    expected = 1656
    assert result == expected


def test_step_till_sync():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_11/day11_test.txt"
    input_test = day11.import_data(INPUT_PATH_TEST)
    result = day11.step_till_sync(input_test)
    expected = 195
    assert result == expected
