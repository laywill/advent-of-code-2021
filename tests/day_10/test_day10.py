import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_10 import day10  # noqa: E402


def test_part1():
    test_incorred_closing_ch_list = ["}", ")", "]", ")", ">"]
    result = day10.part1(test_incorred_closing_ch_list)
    expected = 26397
    assert result == expected


def test_part2():
    test_valid_lines = [
        list("[({([[{{"),
        list("({[<{("),
        list("((((<{<{{"),
        list("<{[{[{{[["),
        list("<{(["),
    ]
    result = day10.part2(test_valid_lines)
    expected = 288957
    assert result == expected


def test_main():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_10/day10_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)
    result_part1, result_part2 = day10.main(input_test)

    expected_part1 = 26397
    assert result_part1 == expected_part1

    expected_part2 = 288957
    assert result_part2 == expected_part2
