#!/usr/bin/env python3.9
"""Solution to Day 17 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_data(path_to_file) -> dict:
    """Load the input file and return a dict with coordinates defining the target."""
    input_str = aoc_file_io.read_file_as_str(path_to_file)

    # Remove human readable guff
    input_list = input_str.strip("target area: ").split(", ")
    x_range_str = input_list[0].strip("x=").split("..")
    y_range_str = input_list[1].strip("y=").split("..")

    # Convert to integers
    x_range = aoc_file_io.list_of_str_to_list_of_int(x_range_str)
    y_range = aoc_file_io.list_of_str_to_list_of_int(y_range_str)

    # Return a dict with keys that make the rest of the code readable
    return {
        "x_min": min(x_range),
        "x_max": max(x_range),
        "y_min": min(y_range),
        "y_max": max(y_range),
    }


def process_step(position: dict, velocity: dict):
    """Return new position and velocity after one update step."""
    new_position = dict(position)
    new_velocity = dict(velocity)

    # Position increases by current velocity
    new_position["x"] += velocity["x"]
    new_position["y"] += velocity["y"]

    # Due to drag, the x velocity changes by 1 toward the value 0
    if velocity["x"] > 0:
        new_velocity["x"] = velocity["x"] - 1
    elif velocity["x"] < 0:
        new_velocity["x"] = velocity["x"] + 1

    # Due to gravity, the y velocity decreases by 1
    new_velocity["y"] = velocity["y"] - 1

    return new_position, new_velocity


def plot_trajectory(x_velocity: int, y_velocity: int, target: dict) -> tuple:
    """
    Return the points of this trajectory and the max height.

    x_velocity  : initial horizontal velocity
    y_velocity  : initial vertical velocity
    target      : coordinates of target
    """
    positions = []
    probe_pos = {"x": 0, "y": 0}
    probe_vel = {"x": int(x_velocity), "y": int(y_velocity)}
    max_y = 0

    while (
        probe_pos["x"] < target["x_max"] and probe_pos["y"] > target["y_min"]
    ):
        probe_pos, probe_vel = process_step(probe_pos, probe_vel)
        positions.append(probe_pos)

        max_y = max(probe_pos["y"], max_y)

    for step in positions:
        if step["x"] <= target["x_max"] and step["x"] >= target["x_min"]:
            if step["y"] <= target["y_max"] and step["y"] >= target["y_min"]:
                return (positions, max_y)

    # Catch all, return None
    return (None, None)


def solve(target_area: dict):
    """Find all possible trajectories that finish in the target, and max height."""
    valid_trajectories = {}

    # Brute force by covering all positive velocities that wont immediately overshoot
    for x_vel in range(0, int(target_area["x_max"]) + 1, 1):

        # Brute force by covering a huge range of velocities. Scale with problem size.
        for y_vel in range(
            -2 * abs(int(target_area["y_max"])),
            2 * abs(int(target_area["y_max"])),
            1,
        ):
            steps, apex = plot_trajectory(x_vel, y_vel, target_area)
            if apex:
                valid_trajectories[tuple([x_vel, y_vel])] = apex
            if steps:
                for step in steps:
                    if (
                        step["x"] <= target_area["x_max"]
                        and step["x"] >= target_area["x_min"]
                    ):
                        if (
                            step["y"] <= target_area["y_max"]
                            and step["y"] >= target_area["y_min"]
                        ):
                            valid_trajectories[tuple([x_vel, y_vel])] = apex

    all_values = valid_trajectories.values()
    max_y_possible = max(all_values)
    print("Part1 - ", max_y_possible)
    print("Part2 - ", len(all_values))
    return max_y_possible, len(all_values)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_17/day17_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_17/day17_real.txt"

    # input_test = import_data(INPUT_PATH_TEST)
    input_real = import_data(INPUT_PATH_DATA)

    # solve(input_test)
    solve(input_real)
