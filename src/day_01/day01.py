#!/usr/bin/env python3.9
"""Solution to Day 1 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def part1(p1_report: list[int]) -> int:
    """Return number of times a list element is greater than the one adjacent."""
    count = -1  # There is no measurement before the first measurement
    prev_val = 0
    for val in p1_report:
        if val > prev_val:
            count += 1
        prev_val = val
    return count


def part2(p2_report: list[int]) -> int:
    """Take a rolling average of a list, return count of times rolling average n > n+1."""
    list_of_windows = []
    for i in range(len(p2_report) - 2):
        sum_total = sum(p2_report[i : i + 3])
        list_of_windows.append(sum_total)

    return part1(list_of_windows)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_01/day01_real.txt"

    # Read the files and convert to lists of ints
    report_real = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_DATA)

    print("Day 01 - Part 1:", part1(report_real))
    print("Day 01 - Part 2:", part2(report_real))
