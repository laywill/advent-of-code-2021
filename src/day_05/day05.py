#!/usr/bin/env python3.9
"""Solution to Day 5 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_vectors(path_to_file) -> [tuple[tuple[int]]]:
    """
    Read the file, and return a list of lines as start and end coordinates.

    Return a list of tuples, each contaiing a pair of tuples.
    """
    input_list = aoc_file_io.read_file_as_list_of_str(path_to_file)
    parsed_input_list = []
    for line in input_list:
        line = line.split(" -> ")
        line = [line[0].split(","), line[1].split(",")]
        for idx, _ in enumerate(line):
            line[idx] = tuple(
                aoc_file_io.list_of_str_to_list_of_int(line[idx])
            )
        parsed_input_list.append(tuple(line))
    return parsed_input_list


def find_size_of_array(input_vectors) -> tuple:
    """Return a tuple of the max_x and max_y coords in vector list."""
    max_x_lhs = max(max(input_vectors, key=lambda x: x[0][0]))[0]
    max_x_rhs = max(max(input_vectors, key=lambda x: x[1][0]))[0]
    max_y_rhs = max(max(input_vectors, key=lambda y: y[0][1]))[1]
    max_y_lhs = max(max(input_vectors, key=lambda y: y[1][1]))[1]

    max_x = max([max_x_lhs, max_x_rhs])
    max_y = max([max_y_lhs, max_y_rhs])
    print("max_x:", max_x)
    print("max_y:", max_y)
    return tuple([max_x + 2, max_y + 2])


def process_vectors(vectors, process_diagonals: bool) -> int:
    """
    Return the number of crossing points in a list of vectors as int.

    Always considers horizontal and vertical lines.
    Will consider diagonal lines too if process_diagonals == True.__abs__
    """
    board = np.zeros(find_size_of_array(vectors), dtype=int)
    for line in vectors:

        # Process vertical lines
        if line[0][0] == line[1][0]:
            max_y = max([line[0][1], line[1][1]])
            min_y = min([line[0][1], line[1][1]])
            for y_coord in range(min_y, max_y + 1, 1):
                board[y_coord, line[0][0]] += 1

        # Process horizontal lines
        elif line[0][1] == line[1][1]:
            max_x = max([line[0][0], line[1][0]])
            min_x = min([line[0][0], line[1][0]])
            for x_coord in range(min_x, max_x + 1, 1):
                board[line[0][1], x_coord] += 1

        elif process_diagonals:
            delta_x = line[1][0] - line[0][0]
            delta_y = line[1][1] - line[0][1]

            step_x = int(delta_x / abs(delta_x))
            step_y = int(delta_y / abs(delta_y))

            # Can assume only 45 degree lines
            coords_x = []
            coords_y = []

            for d_x in range(line[0][0], line[1][0] + step_x, step_x):
                coords_x.append(d_x)
            for d_y in range(line[0][1], line[1][1] + step_y, step_y):
                coords_y.append(d_y)
            for idx, _ in enumerate(coords_x):
                board[coords_y[idx], coords_x[idx]] += 1

        else:
            pass

    number_of_crossing_points = board > 1
    return number_of_crossing_points.sum()


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    INPUT_PATH_DATA = Path.cwd() / "./data/day_05/day05_real.txt"
    input_real = import_vectors(INPUT_PATH_DATA)

    print("Day 05 - Part 1:", process_vectors(input_real, False))
    print()
    print("Day 05 - Part 2:", process_vectors(input_real, True))
