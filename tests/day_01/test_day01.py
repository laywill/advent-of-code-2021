import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402
from src.day_01 import day01  # noqa: E402


def test_part1():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_01/day01_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST)

    result = day01.part1(test_input)
    expected = 7

    assert result == expected


def test_part2():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_01/day01_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST)

    result = day01.part2(test_input)
    expected = 5

    assert result == expected
