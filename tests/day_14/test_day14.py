import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402
from src.day_14 import day14  # noqa: E402


def test_import_data():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    result_polymer, result_insertions_dict = day14.import_data(INPUT_PATH_TEST)

    expected_polymer = "NNCB"
    assert result_polymer == expected_polymer

    expected_insertion_dict = {
        "CH": "B",
        "HH": "N",
        "CB": "H",
        "NH": "C",
        "HB": "C",
        "HC": "B",
        "HN": "C",
        "NN": "C",
        "BH": "H",
        "NC": "B",
        "NB": "B",
        "BN": "B",
        "BB": "N",
        "BC": "B",
        "CC": "N",
        "CN": "C",
    }
    assert result_insertions_dict == expected_insertion_dict


def test_polymer_to_dict_summary():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    result_polymer, result_insertions_dict = day14.import_data(INPUT_PATH_TEST)
    result_polymer_dict = day14.polymer_to_dict_summary(result_polymer)
    expected = {'NN': 1, 'NC': 1, 'CB': 1}
    assert result_polymer_dict == expected


def test_update_summart_dict():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    result_polymer, result_insertions_dict = day14.import_data(INPUT_PATH_TEST)
    result_polymer_dict = day14.polymer_to_dict_summary(result_polymer)
    result_polymer_dict = day14.update_summary_dict(
        result_polymer_dict, result_insertions_dict
    )
    expected = {'NC': 1, 'CN': 1, 'NB': 1, 'BC': 1, 'CH': 1, 'HB': 1}
    assert result_polymer_dict == expected


def test_steps():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    result_polymer, result_insertions_dict = day14.import_data(INPUT_PATH_TEST)
    result_polymer_dict = day14.polymer_to_dict_summary(result_polymer)

    result_polymer_dict2 = day14.steps(
        result_polymer_dict, result_insertions_dict, 2
    )
    expected2 = {
        'NB': 2,
        'BC': 2,
        'CC': 1,
        'CN': 1,
        'BB': 2,
        'CB': 2,
        'BH': 1,
        'HC': 1,
    }
    assert result_polymer_dict2 == expected2

    result_polymer_dict3 = day14.steps(
        result_polymer_dict, result_insertions_dict, 3
    )
    expected3 = {
        'NB': 4,
        'BB': 4,
        'BC': 3,
        'CN': 2,
        'NC': 1,
        'CC': 1,
        'BN': 2,
        'CH': 2,
        'HB': 3,
        'BH': 1,
        'HH': 1,
    }
    assert result_polymer_dict3 == expected3

    result_polymer_dict4 = day14.steps(
        result_polymer_dict, result_insertions_dict, 4
    )
    expected4 = {
        'NB': 9,
        'BB': 9,
        'BN': 6,
        'BC': 4,
        'CC': 2,
        'CN': 3,
        'NC': 1,
        'CB': 5,
        'BH': 3,
        'HC': 3,
        'HH': 1,
        'HN': 1,
        'NH': 1,
    }
    assert result_polymer_dict4 == expected4

    result_polymer_dict10 = day14.steps(
        result_polymer_dict, result_insertions_dict, 10
    )
    expected10 = {
        'NB': 796,
        'BB': 812,
        'BN': 735,
        'BC': 120,
        'CC': 60,
        'CN': 102,
        'NC': 42,
        'CB': 115,
        'BH': 81,
        'HC': 76,
        'HH': 32,
        'HN': 27,
        'NH': 27,
        'CH': 21,
        'HB': 26,
    }
    assert result_polymer_dict10 == expected10


def test_count_chars():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    result_polymer, result_insertions_dict = day14.import_data(INPUT_PATH_TEST)
    result_polymer_dict = day14.polymer_to_dict_summary(result_polymer)

    result_polymer_dict = day14.steps(
        result_polymer_dict, result_insertions_dict, 10
    )
    result_chars = day14.count_chars(result_polymer_dict, result_polymer)
    assert result_chars == 1588

    result_polymer_dict = day14.steps(
        result_polymer_dict, result_insertions_dict, 30
    )
    result_chars = day14.count_chars(result_polymer_dict, result_polymer)
    assert result_chars == 2188189693529
