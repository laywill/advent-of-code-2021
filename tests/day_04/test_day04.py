import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.day_04 import day04  # noqa: E402


def test_import_bingo_data():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_04/day04_test.txt"

    result_calls, result_boards = day04.import_bingo_data(INPUT_PATH_TEST)
    expected_calls = [
        7,
        4,
        9,
        5,
        11,
        17,
        23,
        2,
        0,
        14,
        21,
        24,
        10,
        16,
        13,
        6,
        15,
        25,
        12,
        22,
        18,
        20,
        8,
        19,
        3,
        26,
        1,
    ]

    board1 = np.array(
        [
            (22, 13, 17, 11, 0),
            (8, 2, 23, 4, 24),
            (21, 9, 14, 16, 7),
            (6, 10, 3, 18, 5),
            (1, 12, 20, 15, 19),
        ],
        dtype=int,
    )

    board2 = np.array(
        [
            (3, 15, 0, 2, 22),
            (9, 18, 13, 17, 5),
            (19, 8, 7, 25, 23),
            (20, 11, 10, 24, 4),
            (14, 21, 16, 12, 6),
        ],
        dtype=int,
    )

    board3 = np.array(
        [
            (14, 21, 17, 24, 4),
            (10, 16, 15, 9, 19),
            (18, 8, 23, 26, 20),
            (22, 11, 13, 6, 5),
            (2, 0, 12, 3, 7),
        ],
        dtype=int,
    )

    expected_boards = np.array([board1, board2, board3])

    assert result_calls == expected_calls
    assert result_boards.shape == expected_boards.shape
    assert (result_boards == expected_boards).all()


def test_check_if_won():
    card_no_win = np.array(
        [
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
        ],
        dtype=int,
    )
    result = day04.check_if_won(card_no_win)
    expected = False
    assert result == expected

    card_win_col = np.array(
        [
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
        ],
        dtype=int,
    )
    result = day04.check_if_won(card_win_col)
    expected = True
    assert result == expected

    card_win_row = np.array(
        [
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (1, 1, 1, 1, 1),
            (0, 0, 0, 0, 0),
        ],
        dtype=int,
    )
    result = day04.check_if_won(card_win_row)
    expected = True
    assert result == expected


def test_check_if_anyone_won():
    marked_card1 = np.array(
        [
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (0, 0, 0, 0, 0),
        ],
        dtype=int,
    )

    marked_card2 = np.array(
        [
            (0, 1, 0, 0, 0),
            (0, 1, 0, 0, 0),
            (0, 0, 0, 0, 0),
            (1, 1, 1, 1, 0),
            (0, 1, 0, 0, 0),
        ],
        dtype=int,
    )

    marked_card3 = np.array(
        [
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
            (0, 0, 1, 0, 0),
        ],
        dtype=int,
    )

    cards = np.array([marked_card1, marked_card2, marked_card3])
    result = day04.check_if_anyone_won(cards)
    expected = 2
    assert result == expected


def test_calculate_score():
    card = np.array(
        [
            (22, 13, 17, 11, 0),
            (8, 2, 23, 4, 24),
            (21, 9, 14, 16, 7),
            (6, 10, 3, 18, 5),
            (1, 12, 20, 15, 19),
        ],
        dtype=int,
    )

    card_marked = np.array(
        [
            (1, 0, 1, 0, 1),
            (0, 1, 1, 1, 0),
            (0, 0, 1, 0, 0),
            (0, 1, 1, 0, 1),
            (1, 0, 1, 1, 0),
        ],
        dtype=int,
    )

    result = day04.calculate_score(card, card_marked)
    expected = 164
    assert result == expected


def test_part1():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_04/day04_test.txt"

    # Read the files and convert to lists of ints
    calls_test, boards_test = day04.import_bingo_data(INPUT_PATH_TEST)

    result = day04.part1(calls_test, boards_test)
    expected = 4512
    assert result == expected


def test_part2():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_04/day04_test.txt"

    # Read the files and convert to lists of ints
    calls_test, boards_test = day04.import_bingo_data(INPUT_PATH_TEST)

    result = day04.part2(calls_test, boards_test)
    expected = 1924
    assert result == expected
