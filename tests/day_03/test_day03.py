import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402
from src.day_03 import day03  # noqa: E402


def test_most_common():
    test_data = ["1", "1", "0", "1", "1", "0"]
    result = day03.most_common(test_data)
    expected = "1"
    assert result == expected


def test_least_common():
    test_data = ["1", "1", "0", "1", "1", "0"]
    result = day03.least_common(test_data)
    expected = "0"
    assert result == expected


def test_part1():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_03/day03_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)

    result = day03.part1(test_input)
    expected = 198

    assert result == expected


def test_apply_filter():
    test_data = [
        "00100",
        "11110",
        "10110",
        "10111",
        "10101",
        "01111",
        "00111",
        "11100",
        "10000",
        "11001",
        "00010",
        "01010",
    ]

    result = day03.apply_filter(test_data, 0, "0")
    expected = ["00100", "01111", "00111", "00010", "01010"]

    assert result == expected


def test_filter_to_get_rating():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_03/day03_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)

    result = day03.filter_to_get_rating(test_input, day03.most_common)
    expected = "10111"
    assert result == expected

    result = day03.filter_to_get_rating(test_input, day03.least_common)
    expected = "01010"
    assert result == expected


def test_oxygen_str():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_03/day03_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)

    result = day03.oxygen_str(test_input)
    expected = "10111"

    assert result == expected


def test_co2_str():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_03/day03_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)

    result = day03.co2_str(test_input)
    expected = "01010"

    assert result == expected


def test_part2():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_03/day03_test.txt"
    test_input = aoc_file_io.read_file_as_list_of_str(INPUT_PATH_TEST)

    result = day03.part2(test_input)
    expected = 230

    assert result == expected
