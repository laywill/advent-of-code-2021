import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.day_02 import day02  # noqa: E402


def test_read_file_as_list():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_02/day02_test.txt"

    result = day02.read_file_as_list(INPUT_PATH_TEST)
    expected = [
        ("forward", 5),
        ("down", 5),
        ("forward", 8),
        ("up", 3),
        ("down", 8),
        ("forward", 2),
    ]

    assert result == expected


def test_part1():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_02/day02_test.txt"
    test_input = day02.read_file_as_list(INPUT_PATH_TEST)

    result = day02.part1(test_input)
    expected = 150

    assert result == expected


def test_part2():
    # Define the example data file and the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_02/day02_test.txt"
    test_input = day02.read_file_as_list(INPUT_PATH_TEST)

    result = day02.part2(test_input)
    expected = 900

    assert result == expected
