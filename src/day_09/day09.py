#!/usr/bin/env python3.9
"""Solution to Day 9 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def split(word):
    """Split string into characters."""
    return list(word)


def import_data(path_to_file) -> np.ndarray:
    """Load the input file and return a numpy array of ints."""
    input_as_list_of_str = aoc_file_io.read_file_as_list_of_str(path_to_file)
    board = np.array(
        [
            [int(num) for num in split(line.strip())]
            for line in input_as_list_of_str
        ]
    )
    return board


def pad_array(array_to_pad: np.ndarray) -> np.array:
    """Take a numpy array, and pad the outside."""
    return np.pad(array_to_pad, ((1, 1), (1, 1)), 'maximum')


def main(input_array: np.ndarray):
    """Find all the low points."""
    # Pad the array with big numbers around the edge.
    search_array = pad_array(input_array)
    a_shape = search_array.shape

    # Somewhere to mark if a square belongs to a basin numbers
    basin_nums_array = np.zeros_like(search_array, dtype=int)

    minima_values = []
    minima_coords = []
    for (y, x), _ in np.ndenumerate(
        search_array
    ):  # pylint: disable=invalid-name
        if x in [0, a_shape[1] - 1]:
            # Skip rest of loop
            continue
        if y in [0, a_shape[0] - 1]:
            # Skip rest of loop
            continue

        elem_u = search_array[y, x - 1]
        elem_d = search_array[y, x + 1]
        elem_l = search_array[y - 1, x]
        elem_r = search_array[y + 1, x]

        if search_array[y, x] < min([elem_u, elem_d, elem_l, elem_r]):
            minima_values.append(search_array[y, x])
            minima_coords.append([y, x])

    part1_answer = sum(minima_values) + len(minima_values)
    print("Day 09 - Part 1:", part1_answer)

    coords_checked = set()
    coords_to_check = list(minima_coords)
    while len(coords_to_check) > 0:

        currently_checking = tuple(coords_to_check.pop(0))
        if currently_checking in coords_checked:
            continue
        else:
            coords_checked.add(currently_checking)
            # print("currently checking:", currently_checking)

            y = currently_checking[0]
            x = currently_checking[1]
            if search_array[y, x] == 9:
                # Skip as we have reached a boundary
                continue

            if basin_nums_array[y, x] > 0:
                continue

            coords_to_check.append([y, x - 1])
            coords_to_check.append([y, x + 1])
            coords_to_check.append([y - 1, x])
            coords_to_check.append([y + 1, x])

            basin_elem_u = basin_nums_array[y, x - 1]
            basin_elem_d = basin_nums_array[y, x + 1]
            basin_elem_l = basin_nums_array[y - 1, x]
            basin_elem_r = basin_nums_array[y + 1, x]

            nearby_basin = max(
                [
                    basin_elem_u,
                    basin_elem_d,
                    basin_elem_l,
                    basin_elem_r,
                ]
            )
            if nearby_basin > 0:
                # We are adjacent to an existing basin
                basin_nums_array[y, x] = nearby_basin
            else:
                # start a new basin
                basin_nums_array[y, x] = np.amax(basin_nums_array) + 1

    # Find the number of squares covered by each basin
    count_arr = []
    for basin in range(1, np.amax(basin_nums_array) + 1):
        count_arr.append(np.count_nonzero(basin_nums_array == basin))

    # Sort from smallest to largest
    count_arr.sort()

    part2_answer = count_arr[-1] * count_arr[-2] * count_arr[-3]
    print("Day 09 - Part 2:", part2_answer)

    return tuple([part1_answer, part2_answer])


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_09/day09_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_09/day09_real.txt"

    # input_test = import_data(INPUT_PATH_TEST)
    input_real = import_data(INPUT_PATH_DATA)

    # main(input_test)
    main(input_real)
