#!/bin/bash

# Safer bash scripts by causing it to fail fast, safely and early.
set -euo pipefail

# In this project we will prefer the use of out-of-tree building and execution.
# This allows for cleaner commits, and easier excluding of files and folders from linters.

# Create top level folders
mkdir "./src/"
mkdir "./tests/"
mkdir "./data/"
mkdir "./output/"

# Create a location for common code that will be reusued, e.g. file reading
mkdir "./src/common/"
mkdir "./tests/common/"

# Create folders for each of the days
for day in {01..25}
do
    mkdir "./src/day_${day}/"
    mkdir "./tests/day_${day}/"
    mkdir "./data/day_${day}/"
    mkdir "./output/day_${day}/"
done
