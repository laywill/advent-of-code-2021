#!/usr/bin/env python3.9
"""Solution to Day 4 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_bingo_data(path_to_file):
    """
    Load the input file and return a list of ints (calls) and a list of boards.

    Calls are a list of ints.
    Each board is a list of lists of ints.
    """
    # Read the file, first element is calls, followed by boards
    input_as_list = aoc_file_io.read_file_as_list_of_str(path_to_file, "\n\n")

    calls = input_as_list.pop(0)
    calls = aoc_file_io.str_to_list_of_str(calls, ",")
    calls = aoc_file_io.list_of_str_to_list_of_int(calls)

    boards = []
    for board in input_as_list:
        board = np.array(
            [
                [int(num) for num in line.strip().split()]
                for line in board.split("\n")
            ]
        )
        boards.append(board)
    boards = np.array(boards)
    return calls, boards


def check_if_won(marked_card) -> bool:
    """
    Return True or False Bool if a Bingo card has won.

    A Bingo card has won if 5 numbers in any row or column have been marked off.
    """
    # Check Rows
    if 5 in marked_card.sum(axis=0):
        return True

    # Check Cols
    if 5 in marked_card.sum(axis=1):
        return True

    return False


def check_if_anyone_won(boards) -> int:
    """Return the index of which board won if one has won, else None."""
    for index, board in enumerate(boards):
        if check_if_won(board):
            return index
    return None


def calculate_score(card, marked_card) -> int:
    """Return the sum of all unmarked values on a bingo card."""
    # Swap all the "1" elements for "0" elements using np.where
    # np.where(condition[, x, y])¶ returns elements
    #     chosen from x or y depending on condition.
    marked_card = np.where(marked_card == 1, -1, marked_card)
    marked_card = np.where(marked_card == 0, 1, marked_card)
    marked_card = np.where(marked_card == -1, 0, marked_card)

    # Multiply the two matricies element wise to filter out the marked values
    unmarked_nums = np.multiply(card, marked_card)

    # Sum all the values
    return np.sum(unmarked_nums)


# def part1(calls: list[int], cards, scores) -> int:
def part1(calls: list[int], cards) -> int:
    """
    Find the Bingo card that will win first.

    Return the sum of all the unmarked numbers on the winning Bingo card
    mutiplied by the number that was just called when the board won.
    """
    scores = [np.zeros_like(cards[0], dtype=int)] * len(cards)
    scores = np.array(scores)

    winner = None
    last_call = None
    for call in calls:
        # Get some arrays of rows and cols where the value can be found
        result = np.where(cards == call)

        # zip the 2 arrays to get the exact coordinates
        list_of_coordinates = list(zip(result[0], result[1], result[2]))

        # Mark off all the elements that have been scored
        for coord in list_of_coordinates:
            scores[coord] = 1

        # Check if we have a winner yet
        winner = check_if_anyone_won(scores)
        if winner:
            last_call = call
            break

    winner_unmarked_total = calculate_score(cards[winner], scores[winner])

    return winner_unmarked_total * last_call


# def part2(calls: list[int], cards, scores) -> int:
def part2(calls: list[int], cards) -> int:
    """
    Find the Bingo card that will win last.

    Return the sum of all the unmarked numbers on the losing Bingo card
    mutiplied by the number that was just called when the card finally won.
    """
    scores = [np.zeros_like(cards[0], dtype=int)] * len(cards)
    scores = np.array(scores)

    loser = None
    last_call = None
    yet_to_win = list(range(0, len(cards)))

    for call in calls:
        # Get some arrays of rows and cols where the value can be found
        result = np.where(cards == call)

        # zip the 2 arrays to get the exact coordinates
        list_of_coordinates = list(zip(result[0], result[1], result[2]))

        # Mark off all the elements that have been scored
        for coord in list_of_coordinates:
            scores[coord] = 1

        # Check if we know the loser
        for index, card in enumerate(scores):
            if check_if_won(card):
                # return index
                if index in yet_to_win:
                    yet_to_win.remove(index)

        if len(yet_to_win) == 1:
            loser = yet_to_win[0]
        if len(yet_to_win) == 0:
            last_call = call
            break

    loser_unmarked_total = calculate_score(cards[loser], scores[loser])
    print("loser_unmarked_total", loser_unmarked_total)
    print("last_call", last_call)
    return loser_unmarked_total * last_call


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the example data file and the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_04/day04_real.txt"

    # Read the files and convert to lists of ints
    calls_real, boards_real = import_bingo_data(INPUT_PATH_DATA)
    print("Day 04 - Part 1:", part1(calls_real, boards_real))

    calls_real, boards_real = import_bingo_data(INPUT_PATH_DATA)
    print("Day 04 - Part 2:", part2(calls_real, boards_real))
