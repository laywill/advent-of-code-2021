import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_09 import day09  # noqa: E402


def test_split():
    test_word = "girraffe"
    result = day09.split(test_word)
    expected = ["g", "i", "r", "r", "a", "f", "f", "e"]
    assert result == expected


def test_import_data():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_09/day09_test.txt"
    result = day09.import_data(INPUT_PATH_TEST)
    expected = np.array(
        [
            (2, 1, 9, 9, 9, 4, 3, 2, 1, 0),
            (3, 9, 8, 7, 8, 9, 4, 9, 2, 1),
            (9, 8, 5, 6, 7, 8, 9, 8, 9, 2),
            (8, 7, 6, 7, 8, 9, 6, 7, 8, 9),
            (9, 8, 9, 9, 9, 6, 5, 6, 7, 8),
        ],
        dtype=int,
    )
    assert (result == expected).all()


def test_pad_array():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_09/day09_test.txt"
    input_test = day09.import_data(INPUT_PATH_TEST)
    result = day09.pad_array(input_test)
    expected = np.array(
        [
            (9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9),
            (9, 2, 1, 9, 9, 9, 4, 3, 2, 1, 0, 9),
            (9, 3, 9, 8, 7, 8, 9, 4, 9, 2, 1, 9),
            (9, 9, 8, 5, 6, 7, 8, 9, 8, 9, 2, 9),
            (9, 8, 7, 6, 7, 8, 9, 6, 7, 8, 9, 9),
            (9, 9, 8, 9, 9, 9, 6, 5, 6, 7, 8, 9),
            (9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9),
        ],
        dtype=int,
    )
    assert (result == expected).all()


def test_main():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_09/day09_test.txt"
    input_test = day09.import_data(INPUT_PATH_TEST)
    result_part1, result_part2 = day09.main(input_test)

    expected_part1 = 15
    expected_part2 = 1134

    assert result_part1 == expected_part1
    assert result_part2 == expected_part2
