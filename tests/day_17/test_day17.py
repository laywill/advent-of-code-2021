import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402
from src.day_17 import day17  # noqa: E402


def test_import_data():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_17/day17_test.txt"
    result = day17.import_data(INPUT_PATH_TEST)
    expected = {
        "x_min": 20,
        "x_max": 30,
        "y_min": -10,
        "y_max": -5,
    }
    assert result == expected


def test_process_step():
    probe_pos = {"x": 0, "y": 0}
    probe_vel = {"x": 7, "y": 2}
    result_pos, result_vel = day17.process_step(probe_pos, probe_vel)
    expected_pos = {"x": 7, "y": 2}
    expected_vel = {"x": 6, "y": 1}
    assert result_pos == expected_pos
    assert result_vel == expected_vel

    probe_pos = dict(expected_pos)
    probe_vel = dict(expected_vel)
    result_pos, result_vel = day17.process_step(probe_pos, probe_vel)
    expected_pos = {"x": 13, "y": 3}
    expected_vel = {"x": 5, "y": 0}
    assert result_pos == expected_pos
    assert result_vel == expected_vel

    probe_pos = dict(expected_pos)
    probe_vel = dict(expected_vel)
    result_pos, result_vel = day17.process_step(probe_pos, probe_vel)
    expected_pos = {"x": 18, "y": 3}
    expected_vel = {"x": 4, "y": -1}
    assert result_pos == expected_pos
    assert result_vel == expected_vel


def test_plot_trajectory():
    test_target = {
        "x_min": 20,
        "x_max": 30,
        "y_min": -10,
        "y_max": -5,
    }
    test_x_vel = 6
    test_y_vel = 9
    result_pos, result_apex = day17.plot_trajectory(
        test_x_vel, test_y_vel, test_target
    )
    assert result_apex == 45


def test_solve():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_17/day17_test.txt"
    input_test = day17.import_data(INPUT_PATH_TEST)
    result_max_y, result_num_trajectories = day17.solve(input_test)
    expected_max_y = 45
    expected_num_trajectories = 112

    assert result_max_y == expected_max_y
    assert result_num_trajectories == expected_num_trajectories
