#!/bin/bash

# Safer bash scripts by causing it to fail fast, safely and early.
set -euo pipefail

# shellcheck disable=SC2061
for FILE in $(find ./ -name ./*.sh | sort)
do
    echo -e "\nLinting $FILE with Shellcheck"
    shellcheck "$FILE"
done
