#!/usr/bin/env python3.9
"""Solution to Day 14 of AoC 2021."""

from pathlib import Path


def import_data(path_to_file):
    """Load the input file and return a polymer template and list of pair insertions."""
    # Read the file, first element is calls, followed by boards
    if path_to_file.is_file():
        with open(path_to_file, "r", encoding="utf-8") as input_file:
            # Get the initial polymer
            polymer = input_file.readline().strip()

            # Remove the blank line
            input_file.readline()

            # Create dict of insertions
            insertions = {}
            for line in input_file.readlines():
                key, value = line.strip().split(" -> ")
                insertions[key] = value
    else:
        raise FileNotFoundError("The file path does not exist:", path_to_file)

    return polymer, insertions


def polymer_to_dict_summary(polymer: str) -> dict:
    """Return dict that summarises the count of each pairs."""
    pairs_dict = {}
    for i in range(len(polymer) - 1):
        pair = polymer[i] + polymer[i + 1]
        if pair in pairs_dict:
            pairs_dict[pair] += 1
        else:
            pairs_dict[pair] = 1
    return pairs_dict


def update_summary_dict(polymer_dict: dict, insertion_rules: dict) -> dict:
    """Return a new dict summarising the number each pair after inserting new items."""
    new_pairs_dict = {}
    for key, value in polymer_dict.items():

        # Get the character to insert by looking up the key
        ch_to_insert = insertion_rules.get(key)

        new_pair_l = key[0] + ch_to_insert
        new_pair_r = ch_to_insert + key[1]

        if new_pair_l in new_pairs_dict:
            new_pairs_dict[new_pair_l] += value
        else:
            new_pairs_dict[new_pair_l] = value

        if new_pair_r in new_pairs_dict:
            new_pairs_dict[new_pair_r] += value
        else:
            new_pairs_dict[new_pair_r] = value

    return new_pairs_dict


def steps(
    polymer_summary: dict, insertions_dict: dict, num_steps: int
) -> dict:
    """Return the polymer summary_dict updated after N rounds of insertions."""
    for _ in range(num_steps):
        polymer_summary = update_summary_dict(polymer_summary, insertions_dict)
    return polymer_summary


def count_chars(polymer_summary: dict, polymer: str) -> int:
    """Return the difference between occurrences of most and least common characters."""
    char_counts = {}

    keys = list(polymer_summary.keys())
    for key in keys:
        if key[0] in char_counts:
            char_counts[key[0]] += polymer_summary[key]
        else:
            char_counts[key[0]] = polymer_summary[key]

        if key[1] in char_counts:
            char_counts[key[1]] += polymer_summary[key]
        else:
            char_counts[key[1]] = polymer_summary[key]

    # Add an extra entry for the first and last characters
    char_counts[polymer[0]] += 1
    char_counts[polymer[-1]] += 1

    # Sort the dict from smallest to largest values
    # The values in here will be 2x too big because we have counted characters twice
    # once as the right hand of a pair, once as the left hand of a pair
    total_counts = sorted(char_counts.values())

    return int(round((total_counts[-1] - total_counts[0]) / 2))


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    INPUT_PATH_TEST = Path.cwd() / "./data/day_14/day14_test.txt"
    # INPUT_PATH_DATA = Path.cwd() / "./data/day_14/day14_real.txt"

    polymer_test, insertions_dict_test = import_data(INPUT_PATH_TEST)
    # polymer_real, insertions_dict_real = import_data(INPUT_PATH_DATA)

    polymer_dict_test = polymer_to_dict_summary(polymer_test)
    # polymer_dict_real = polymer_to_dict_summary(polymer_real)

    polymer_dict_test = steps(polymer_dict_test, insertions_dict_test, 10)
    # polymer_dict_real = steps(polymer_dict_real, insertions_dict_real, 10)

    print(
        "Day 14 - Test - Part 1:", count_chars(polymer_dict_test, polymer_test)
    )
    # print(
    #     "Day 14 - Real - Part 1:", count_chars(polymer_dict_real, polymer_real)
    # )

    polymer_dict_test = steps(polymer_dict_test, insertions_dict_test, 30)
    # polymer_dict_real = steps(polymer_dict_real, insertions_dict_real, 30)

    print(
        "Day 14 - Test - Part 2:", count_chars(polymer_dict_test, polymer_test)
    )
    # print(
    #     "Day 14 - Real - Part 2:", count_chars(polymer_dict_real, polymer_real)
    # )
