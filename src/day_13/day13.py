#!/usr/bin/env python3.9
"""Solution to Day 13 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_data(path_to_file) -> tuple[list, list]:
    """
    Load the input file and return a list of coordinates and a list of folds.

    Coordinates is a list of tuples of ints.
    The list of folds is a list of axes and indexes on that axis.
    """
    # Read the file, first element is calls, followed by boards
    points_and_folds = aoc_file_io.read_file_as_list_of_str(
        path_to_file, "\n\n"
    )

    # Process Points
    points = list(points_and_folds[0].strip().split("\n"))
    points = [tuple(list(map(int, point.split(",")))) for point in points]

    # Process Folds
    folds = list(points_and_folds[1].split("\n"))
    folds = [fold.strip("fold along ").split("=") for fold in folds]
    folds = [tuple([fold[0], int(fold[1])]) for fold in folds]

    return points, folds


def array_size_from_points(list_of_points: list) -> tuple:
    """Given the input list of points, return tuple of numpy array size."""
    max_x = 0
    max_y = 0
    for coord in list_of_points:
        if coord[0] > max_x:
            max_x = coord[0]
        if coord[1] > max_y:
            max_y = coord[1]
    return (max_y + 1, max_x + 1)


def generate_points_board(list_of_points: list) -> np.ndarray:
    """Return numpy array populated with points given puzzle input list."""
    yx_dimensions = array_size_from_points(list_of_points)
    board = np.zeros(yx_dimensions, dtype=int)

    for coord in list_of_points:
        board[coord[1], coord[0]] += 1

    return board


def fold(points_board: np.ndarray, fold_instruction: tuple) -> np.ndarray:
    """Fold the board in half once."""
    axis_dict = {"x": 1, "y": 0}

    points_board = np.delete(
        points_board, fold_instruction[1], axis=axis_dict[fold_instruction[0]]
    )
    h1, h2 = np.array_split(
        points_board, 2, axis=axis_dict[fold_instruction[0]]
    )

    if fold_instruction[0] == "x":
        h2 = np.flip(h2, axis=1)
    elif fold_instruction[0] == "y":
        h2 = np.flip(h2, axis=0)
    else:
        print("Something went badly wrong...")
        exit(1)

    return np.add(h1, h2)


def do_folding(
    points_board: np.ndarray, fold_instruction_list: list
) -> np.ndarray:
    """Fold the board as specified in fold instruction list."""
    while fold_instruction_list:
        points_board = fold(points_board, fold_instruction_list.pop(0))
    return points_board


def format_output(points_board: np.ndarray) -> str:
    """Format the final board into a string that can be printed."""
    a_size = points_board.shape

    list_of_lists = []
    for _ in range(a_size[0]):
        line = [" "] * a_size[1]
        list_of_lists.append(line)

    for index, x in np.ndenumerate(points_board):
        if x > 0:
            list_of_lists[index[0]][index[1]] = "#"

    out_str = ""
    for line in list_of_lists:
        out_str = out_str + "".join(line) + "\n"

    return out_str


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_13/day13_real.txt"

    # input_test_points, input_test_folds = import_data(INPUT_PATH_TEST)
    input_real_points, input_real_folds = import_data(INPUT_PATH_DATA)

    # points_board_test = generate_points_board(input_test_points)
    points_board_real = generate_points_board(input_real_points)

    # points_board_test = fold(points_board_test, input_test_folds.pop(0))
    points_board_real = fold(points_board_real, input_real_folds.pop(0))

    # print("Day 13 - Test - Part 1:", np.count_nonzero(points_board_test))
    print("Day 13 - Real - Part 1:", np.count_nonzero(points_board_real))

    # points_board_test = do_folding(points_board_test, input_test_folds)
    points_board_real = do_folding(points_board_real, input_real_folds)

    # print("Day 13 - Test - Part 2:\n")
    # print(format_output(points_board_test))
    print("Day 13 - Real - Part 2:\n")
    print(format_output(points_board_real))
