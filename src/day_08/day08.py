#!/usr/bin/env python3.9
"""Solution to Day 8 of AoC 2021."""

import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def read_input(file_path) -> list[list[str]]:
    """
    Return list of lists of strings.

    List contains an element for each line.
    Each element is a list of two elements: left and right of the | character.
    Each of these elements is a string representing a 7seg pattern.
    """
    # Read file as list of strings
    data = aoc_file_io.read_file_as_list_of_str(file_path)

    # process the list of strings to break it down
    for idx, _ in enumerate(data):
        data[idx] = data[idx].split("|")
        data[idx][0] = data[idx][0].strip().split(" ")
        data[idx][1] = data[idx][1].strip().split(" ")

    return data


def part1(list_of_patterns: list) -> int:
    """Return integer sum of number of 1,4, 7 and 8 characters appear in outputs."""
    # Define lengths of each unique digit
    len_ch1 = 2
    len_ch4 = 4
    len_ch7 = 3
    len_ch8 = 7

    num_ch1 = 0
    num_ch4 = 0
    num_ch7 = 0
    num_ch8 = 0

    # Count how many unique characters we find
    for _, pattern in enumerate(list_of_patterns):
        for element in pattern[1]:
            if len(element) == len_ch1:
                num_ch1 += 1
            elif len(element) == len_ch4:
                num_ch4 += 1
            elif len(element) == len_ch7:
                num_ch7 += 1
            elif len(element) == len_ch8:
                num_ch8 += 1

    return num_ch1 + num_ch4 + num_ch7 + num_ch8


def by_size(words, size):
    """Return a sub-set of a list, based on the size of words desired."""
    return [word for word in words if len(word) == size]


def create_segment_lookup_dict(
    top: set,
    top_left: set,
    top_right: set,
    middle: set,
    bottom_left: set,
    bottom_right: set,
    bottom: set,
) -> dict:
    """
    Return Dict with segment codes as keys and ints as values.

    Returned dict can be used to deecode each 7 seg string pattern to an integer value.
    """
    ch0 = set.union(
        bottom,
        bottom_left,
        top_left,
        top,
        top_right,
        bottom_right,
    )
    ch1 = set.union(top_right, bottom_right)
    ch2 = set.union(top, top_right, middle, bottom_left, bottom)
    ch3 = set.union(top, top_right, middle, bottom_right, bottom)
    ch4 = set.union(top_left, top_right, middle, bottom_right)
    ch5 = set.union(top, top_left, middle, bottom_right, bottom)
    ch6 = set.union(
        top,
        top_left,
        middle,
        bottom_left,
        bottom_right,
        bottom,
    )
    ch7 = set.union(top, top_right, bottom_right)
    ch8 = set.union(ch0, middle)
    ch9 = set.union(ch7, top_left, middle, bottom)

    ch_lookup = {
        "".join(sorted(list(ch0))): 0,
        "".join(sorted(list(ch1))): 1,
        "".join(sorted(list(ch2))): 2,
        "".join(sorted(list(ch3))): 3,
        "".join(sorted(list(ch4))): 4,
        "".join(sorted(list(ch5))): 5,
        "".join(sorted(list(ch6))): 6,
        "".join(sorted(list(ch7))): 7,
        "".join(sorted(list(ch8))): 8,
        "".join(sorted(list(ch9))): 9,
    }
    return ch_lookup


def parse_right_hand_side(list_of_ten_signals: list[str]) -> dict[str]:
    """
    Return dict with alphabetically segment codes as keys to numberic value represented.

    Accepts a list of ten strings, taken from the right hand side of each line of the puzzle input
    Uses set theory to work out which segment is which.
    Combines sets to create strings representing each number on the 7 segment display.
    Returns a dict of these strings, with the string as the key for an integer value it represents.
    This allows a simple seach by key, return value, to decode a sorted string to an integer value
    """
    # Define lengths of each string representing a 7-segment display digit
    len_ch0 = 6
    len_ch1 = 2
    # len_ch2 = 5
    # len_ch3 = 5
    len_ch4 = 4
    len_ch5 = 5
    # len_ch6 = 6
    len_ch7 = 3
    len_ch8 = 7
    len_ch9 = 6

    # Initialise empty set for each segment
    seg_top = set()  # horizzontal bar across the top
    seg_top_left = set()  # vertical top left
    seg_top_right = set()  # vertical top tight
    seg_middle = set()  # horizontal bar across the middle
    seg_bottom_left = set()  # vertical bottom left
    seg_bottom_right = set()  # vertical bottom right
    seg_bottom = set()  # horizontal bar across the bottom

    # Populate right verticals
    # Only one result, so index directly
    result = by_size(list_of_ten_signals, len_ch1)[0]
    seg_top_right = set(result)
    seg_bottom_right = set(result)

    # Find the top bar
    # Only one result, so index directly
    result = by_size(list_of_ten_signals, len_ch7)[0]
    seg_top = set(result).difference(set(seg_top_right))

    # Populate top left and mid bar
    # Only one result, so index directly
    result = by_size(list_of_ten_signals, len_ch4)[0]
    seg_top_left = set(result).difference(set(seg_top_right))
    seg_middle = set(result).difference(set(seg_top_right))

    # find the 9 amongst the 9,6,0
    result = by_size(list_of_ten_signals, len_ch9)
    # the 9 is the only one that will contain all but one of the segments we know so far
    #  so we know we can find the bottom horizontal bar
    output = []
    for elem in result:
        unique_in_elem = set(elem).difference(
            seg_top_right,
            seg_bottom_right,
            seg_top,
            seg_top_left,
            seg_middle,
        )
        output.append(unique_in_elem)
    for elem in output:
        if len(elem) == 1:
            seg_bottom = elem

    # find the bottom left, only one not tested yet
    # Only one result, so index directly
    result = by_size(list_of_ten_signals, len_ch8)[0]
    seg_bottom_left = set(result).difference(
        seg_top_right,
        seg_bottom_right,
        seg_top,
        seg_top_left,
        seg_middle,
        seg_bottom,
    )

    # Find the bottom right, using the 5 and 2 and 3
    result = by_size(list_of_ten_signals, len_ch5)
    output = []
    for elem in result:
        unique_in_elem = set(elem).difference(
            seg_top, seg_top_left, seg_middle, seg_bottom
        )
        output.append(unique_in_elem)

    for elem in output:
        if len(elem) == 1:
            seg_bottom_right = elem

    # find the top right by filtering out the one we just found
    seg_top_right = set(seg_top_right).difference(seg_bottom_right)

    # find the top left using the 0
    result = by_size(list_of_ten_signals, len_ch0)
    output = []
    for elem in result:
        unique_in_elem = set(elem).difference(
            seg_top,
            seg_top_right,
            seg_bottom,
            seg_bottom_right,
            seg_bottom_left,
        )
        output.append(unique_in_elem)

    for elem in output:
        if len(elem) == 1:
            seg_top_left = elem

    # Find the middle bar by filtering out the one we just found
    seg_middle = set(seg_middle).difference(seg_top_left)

    # Now decode each 7 seg pattern
    ch_lookup = create_segment_lookup_dict(
        top=seg_top,
        top_left=seg_top_left,
        top_right=seg_top_right,
        middle=seg_middle,
        bottom_left=seg_bottom_left,
        bottom_right=seg_bottom_right,
        bottom=seg_bottom,
    )
    return ch_lookup


def create_int_by_concatenating_list_of_ints(list_of_ints: list[int]) -> int:
    """Return an int, created by concatenating the values in list of ints."""
    strings = [str(integer) for integer in list_of_ints]
    a_string = "".join(strings)
    return int(a_string)


def part2(list_of_patterns: list) -> int:
    """Return integer sum of all decoded output values."""
    # Create an empty list to stuff values into for us to sum later
    list_of_vals = []
    for _, pattern in enumerate(list_of_patterns):

        dict_of_keys = parse_right_hand_side(pattern[0])

        value_list = []
        for num in pattern[1]:
            lookup_key = "".join(sorted(num))
            value_list.append(dict_of_keys[lookup_key])

        # Convert list of ints to one int
        value = create_int_by_concatenating_list_of_ints(value_list)
        list_of_vals.append(value)

    return sum(list_of_vals)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_08/day08_real.txt"
    input_real = read_input(INPUT_PATH_DATA)

    print("Day 08 - Part 1:", part1(input_real))
    print()
    print("Day 08 - Part 2:", part2(input_real))
