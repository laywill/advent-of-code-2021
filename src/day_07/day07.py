#!/usr/bin/env python3.9
"""Solution to Day 7 of AoC 2021."""

import os
import sys
from pathlib import Path
from statistics import mean, median

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def part1(list_of_crab_positions: list) -> int:
    """Return the integer amount of fuel needed to align crabs."""
    # Find the optimal position
    position_median = int(median(list_of_crab_positions))

    # Calculate the fuel needed to move crabs to optimum position
    fuel = 0
    for _, crab in enumerate(list_of_crab_positions):
        fuel += int(abs(crab - position_median))
    return fuel


def part2(list_of_crab_positions: list) -> int:
    """
    Return the integer amount of fuel needed to align crabs.

    Since it is now very expensive to move crabs a long way, assume:
    - It will be most efficient to move everything to somewhere near the middle
    - The probability of a position being the solution will be normally distributed
    - The middle of a normal distribution is the mean of the data
    - There will be some noise, so consider +/- a few options from the centre point.
    """
    # Find the Mean
    position_mean = mean(list_of_crab_positions)

    # Add a few extra positions to try either side of mean
    position_options = list(
        range(
            int(round(position_mean) - 1),
            int(round(position_mean) + 1),
        )
    )

    fuel_list = []
    for position in position_options:
        fuel = 0
        for _, crab in enumerate(list_of_crab_positions):
            fuel += int(sum(range(abs(crab - position) + 1)))
        fuel_list.append(fuel)

    return min(fuel_list)


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    INPUT_PATH_DATA = Path.cwd() / "./data/day_07/day07_real.txt"
    input_real = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_DATA, ",")

    print("Day 07 - Part 1:", part1(input_real))
    print()
    print("Day 07 - Part 2:", part2(input_real))
