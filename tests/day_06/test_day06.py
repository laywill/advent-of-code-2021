import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import aoc_file_io  # noqa: E402
from src.day_06 import day06  # noqa: E402


def test_create_dict():
    # Define the example puzzle input data file
    INPUT_PATH_TEST = Path.cwd() / "./data/day_06/day06_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, ",")
    result = day06.create_dict(input_test)
    expected = {0: 0, 1: 1, 2: 1, 3: 2, 4: 1, 5: 0, 6: 0, 7: 0, 8: 0}
    assert result == expected


def test_run_one_day():
    # Define the example puzzle input data file
    INPUT_PATH_TEST = Path.cwd() / "./data/day_06/day06_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, ",")
    result = day06.create_dict(input_test)
    result = day06.run_one_day(result)
    expected = {0: 1, 1: 1, 2: 2, 3: 1, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0}
    assert result == expected


def test_breed_fish():
    # Define the example puzzle input data file
    INPUT_PATH_TEST = Path.cwd() / "./data/day_06/day06_test.txt"
    input_test = aoc_file_io.read_file_as_list_of_int(INPUT_PATH_TEST, ",")

    # part 1
    result = day06.breed_fish(input_test, 80)
    expected = 5934
    assert result == expected

    # part 2
    result = day06.breed_fish(input_test, 256)
    expected = 26984457539
    assert result == expected
