import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.day_05 import day05  # noqa: E402


def test_part1():
    # Define the example puzzle input data file
    INPUT_PATH_TEST = Path.cwd() / "./data/day_05/day05_test.txt"
    input_test = day05.import_vectors(INPUT_PATH_TEST)
    result = day05.process_vectors(input_test, False)
    expected = 5
    assert result == expected


def test_part2():
    # Define the example puzzle input data file
    INPUT_PATH_TEST = Path.cwd() / "./data/day_05/day05_test.txt"
    input_test = day05.import_vectors(INPUT_PATH_TEST)
    result = day05.process_vectors(input_test, True)
    expected = 12
    assert result == expected
