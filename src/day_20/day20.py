#!/usr/bin/env python3.9
"""Solution to Day 20 of AoC 2021."""

import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')

from src.common import (  # pylint: disable=wrong-import-position # noqa: E402
    aoc_file_io,
)


def import_data(path_to_file) -> tuple[list[str], np.ndarray]:
    """Load the input file and return a dict with coordinates defining the target."""
    input_str = aoc_file_io.read_file_as_str(path_to_file)

    # Remove human readable guff
    input_list = input_str.strip().split("\n\n")

    algorithm = list(input_list[0])

    input_image_lines = input_list[1].split("\n")
    input_image = []
    for _, line in enumerate(input_image_lines):
        input_image.append(list(line))

    return algorithm, np.array(input_image)


def pad_image(array_to_pad: np.ndarray, pad_dist: int) -> np.ndarray:
    """Take a numpy array, and pad the outside."""
    return np.array(
        np.pad(
            array_to_pad,
            ((pad_dist, pad_dist), (pad_dist, pad_dist)),
            'constant',
            constant_values=('.'),
        )
    )


def strip_and_pad(array_to_pad: np.ndarray) -> np.ndarray:
    """Strip the edge, then pad. Grow array size by 2 in x and y."""
    out_a = array_to_pad[1:-1, 1:-1]
    out_a = np.pad(out_a, ((2, 2), (2, 2)), mode='edge')
    return np.array(out_a)


def print_image(array_to_print: np.ndarray) -> None:
    """Print image to terminal - for debugging."""
    for row in array_to_print:
        out_str = ''.join(list(row))
        print(out_str)
    print()


def parse_image(
    algorithm: list, array_image: np.ndarray, iterations: int
) -> int:
    """Take a numpy array, analyse groups of pixels, return num points."""
    output_array = np.full(array_image.shape, ".", dtype=str)
    temp_array = np.array(array_image)

    for _ in range(iterations):
        dimensions = temp_array.shape
        for row in range(dimensions[0] - 2):
            for col in range(dimensions[1] - 2):
                arr = temp_array[row : row + 3, col : col + 3]
                arr = np.reshape(arr, 9)

                # Convert # and . to binary
                arr = np.where(arr == '#', "1", "0")

                # Convert 1D array to string
                bin_str = ''.join(list(arr))

                # Convert binary to integer
                index = int(bin_str, 2)
                output_array[row + 1, col + 1] = algorithm[index]

        temp_array = np.array(strip_and_pad(output_array))
        output_array = pad_image(output_array, 1)

    return np.count_nonzero(temp_array == "#")


if __name__ == "__main__":
    print("NB: Run this program from the root directory of the repository.\n")

    # Define the puzzle input data
    # INPUT_PATH_TEST = Path.cwd() / "./data/day_20/day20_test.txt"
    INPUT_PATH_DATA = Path.cwd() / "./data/day_20/day20_real.txt"

    # test_algorithm, test_image = import_data(INPUT_PATH_TEST)
    real_algorithm, real_image = import_data(INPUT_PATH_DATA)

    # test_image = pad_image(test_image, 3)
    real_image = pad_image(real_image, 3)

    # print("Day 20 - Test - Part 1:", parse_image(test_algorithm, test_image, 2))
    print(
        "Day 20 - Real - Part 1:", parse_image(real_algorithm, real_image, 2)
    )

    # print("Day 20 - Test - Part 2:", parse_image(test_algorithm, test_image, 50))
    print(
        "Day 20 - Real - Part 2:", parse_image(real_algorithm, real_image, 50)
    )
