import os
import sys
from pathlib import Path

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_08 import day08  # noqa: E402


def test_read_input():
    INPUT_PATH_TEST_SMALL = Path.cwd() / "./data/day_08/day08_test_small.txt"
    result = day08.read_input(INPUT_PATH_TEST_SMALL)
    expected = [
        [
            [
                'acedgfb',
                'cdfbe',
                'gcdfa',
                'fbcad',
                'dab',
                'cefabd',
                'cdfgeb',
                'eafb',
                'cagedb',
                'ab',
            ],
            ['cdfeb', 'fcadb', 'cdfeb', 'cdbaf'],
        ]
    ]
    assert result == expected


def test_part1():
    INPUT_PATH_TEST_LARGE = Path.cwd() / "./data/day_08/day08_test_large.txt"
    input_test_large = day08.read_input(INPUT_PATH_TEST_LARGE)
    result = day08.part1(input_test_large)
    expected = 26
    assert result == expected


def test_by_size():
    test_list = ["one", "two", "three", "four", "five"]

    result = day08.by_size(test_list, 3)
    expected = ["one", "two"]
    assert result == expected

    result = day08.by_size(test_list, 4)
    expected = ["four", "five"]
    assert result == expected

    result = day08.by_size(test_list, 5)
    expected = ["three"]
    assert result == expected


def test_create_segment_lookup_dict():
    ch_top = {"a"}
    ch_top_left = {"b"}
    ch_top_right = {"c"}
    ch_middle = {"d"}
    ch_bottom_left = {"e"}
    ch_bottom_right = {"f"}
    ch_bottom = {"g"}
    result = day08.create_segment_lookup_dict(
        top=ch_top,
        top_left=ch_top_left,
        top_right=ch_top_right,
        middle=ch_middle,
        bottom_left=ch_bottom_left,
        bottom_right=ch_bottom_right,
        bottom=ch_bottom,
    )
    expected = {
        "abcefg": 0,
        "cf": 1,
        "acdeg": 2,
        "acdfg": 3,
        "bcdf": 4,
        "abdfg": 5,
        "abdefg": 6,
        "acf": 7,
        "abcdefg": 8,
        "abcdfg": 9,
    }
    assert result == expected


def test_parse_right_hand_side():
    INPUT_PATH_TEST_SMALL = Path.cwd() / "./data/day_08/day08_test_small.txt"
    input_list = day08.read_input(INPUT_PATH_TEST_SMALL)
    result = day08.parse_right_hand_side(input_list[0][0])
    expected = {
        'abcdeg': 0,
        'ab': 1,
        'acdfg': 2,
        'abcdf': 3,
        'abef': 4,
        'bcdef': 5,
        'bcdefg': 6,
        'abd': 7,
        'abcdefg': 8,
        'abcdef': 9,
    }
    assert result == expected


def test_part2():
    # Define the puzzle input data
    INPUT_PATH_TEST_SMALL = Path.cwd() / "./data/day_08/day08_test_small.txt"
    input_test_small = day08.read_input(INPUT_PATH_TEST_SMALL)
    result = day08.part2(input_test_small)
    expected = 5353
    assert result == expected

    # Define the puzzle input data
    INPUT_PATH_TEST_LARGE = Path.cwd() / "./data/day_08/day08_test_large.txt"
    input_test_large = day08.read_input(INPUT_PATH_TEST_LARGE)
    result = day08.part2(input_test_large)
    expected = 61229
    assert result == expected
