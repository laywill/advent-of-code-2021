import os
import sys
from pathlib import Path

import numpy as np

current_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, current_path + '/../../')


from src.common import aoc_file_io  # noqa: E402
from src.day_13 import day13  # noqa: E402


def test_import_data():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    result_points, result_folds = day13.import_data(INPUT_PATH_TEST)
    expected_points = [
        (6, 10),
        (0, 14),
        (9, 10),
        (0, 3),
        (10, 4),
        (4, 11),
        (6, 0),
        (6, 12),
        (4, 1),
        (0, 13),
        (10, 12),
        (3, 4),
        (3, 0),
        (8, 4),
        (1, 10),
        (2, 14),
        (8, 10),
        (9, 0),
    ]
    expected_folds = [
        ('y', 7),
        ('x', 5),
    ]
    assert result_points == expected_points
    assert result_folds == expected_folds


def test_array_size_from_points():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    input_test_points, input_test_folds = day13.import_data(INPUT_PATH_TEST)
    result = day13.array_size_from_points(input_test_points)
    expected = (15, 11)
    assert result == expected


def test_generate_points_board():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    input_test_points, input_test_folds = day13.import_data(INPUT_PATH_TEST)
    result = day13.generate_points_board(input_test_points)
    expected = np.array(
        [
            [0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0],
            [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
    )
    assert (result == expected).all


def test_fold():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    input_test_points, input_test_folds = day13.import_data(INPUT_PATH_TEST)
    points_board_test = day13.generate_points_board(input_test_points)
    result = day13.fold(points_board_test, input_test_folds.pop(0))

    expected = np.array(
        [
            [1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0],
            [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 0, 1, 0, 2, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]
    )

    assert (result == expected).all


def test_do_folding():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    input_test_points, input_test_folds = day13.import_data(INPUT_PATH_TEST)
    points_board_test = day13.generate_points_board(input_test_points)
    points_board_test = day13.fold(points_board_test, input_test_folds.pop(0))
    result = day13.do_folding(points_board_test, input_test_folds)

    expected = np.array(
        [
            [1, 1, 1, 1, 1],
            [1, 0, 0, 0, 1],
            [1, 0, 0, 0, 1],
            [1, 0, 0, 0, 1],
            [1, 2, 2, 1, 1],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ]
    )

    assert (result == expected).all


def test_format_output():
    INPUT_PATH_TEST = Path.cwd() / "./data/day_13/day13_test.txt"
    input_test_points, input_test_folds = day13.import_data(INPUT_PATH_TEST)
    points_board_test = day13.generate_points_board(input_test_points)
    points_board_test = day13.fold(points_board_test, input_test_folds.pop(0))
    points_board_test = day13.do_folding(points_board_test, input_test_folds)
    result = day13.format_output(points_board_test)

    expected = (
        "#####\n" "#   #\n" "#   #\n" "#   #\n" "#####\n" "     \n" "     \n"
    )

    assert result == expected
